# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

# Install library if necessary
!pip install pyspark

"""#Ensemble Learning tutorial in scikit learn and pyspark + MLlib

- Let's import the necessary packages
"""

from pyspark import SparkContext
from pyspark.mllib.tree import DecisionTree
from pyspark.mllib.regression import LabeledPoint
sc = SparkContext('local[*]', 'Ensemble learning tutorial') # Star to use all calculation capacities 

# Prepare the data
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import metrics

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier

import numpy as np
from time import time

"""- Let's load and prepare the dataset for scikit learning and ML lib"""

# Load datasets
data = datasets.load_digits()
X=data.data
y=data.target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)
trainingData= [LabeledPoint(y_train[i], list(X_train[i])) for i in range(0,len(y_train))]
trainingData = sc.parallelize(trainingData)
testData = [LabeledPoint(y_test[i], list(X_test[i])) for i in range(0,len(y_test))]
testData = sc.parallelize(testData)

"""#Simple Decision Tree

- In scikit-learn
"""

# If big difference between scikit learn and pySpark, it means that there is probably something happening
time0 = time()
DT = DecisionTreeClassifier()
DT.fit(X_train, y_train)
y_pred = DT.predict(X_test) # We do the prediction
print(metrics.accuracy_score(y_test, y_pred)) # We calculate the accuracy
print("time: %s seconds" %(np.round(time()-time0,decimals=2))) # We display the execution time (fast because not a big dataset)

"""#In pySpark +  MLlib"""

time0 = time()
# Train a decisionTree model
# Note that empty categoricalFeaturesInfo {} indicates all features are continuous
model = DecisionTree.trainClassifier(trainingData,numClasses=len(set(y)), categoricalFeaturesInfo={},
                                     impurity='gini')#,maxDepth=5 # gini index to measure the impurity to chose which attribute is the best at this time (Shanon entropy another measure and there are others)
predictions = model.predict(testData.map(lambda x: x.features))
accuracy = metrics.accuracy_score(y_test, predictions.collect())
print('DT Accuracy =' + str(accuracy))

print("time: %s seconds" %(np.round(time()-time0, decimals=2)))

# Time 0.02 seconds with scikit learn while with spark it's 3.15 seconds
# Not so much computations so we lose a little bit in the beginning (in terms of costs), that's why we lose some time
# Also the fact that we don't have a big dataset because Spark is better when used on large data sets

"""- Optionnal"""

# Collect and check the predictions
labelsAndPredictions = testData.map(lambda lp: lp.label).zip(predictions)
labelsAndPredictions.collect() # Real gains and predicted gains

# For each instance, its true class is the predicted class or not. For ex class 2 predicted as class 1 so it's wrong, class 8 predicted as class 9 so it's wrong,
# Then class 2 predicated as class 2 it's correct, etc

# Filter only the false predictions
labelsAndPredictions.filter(lambda lp: lp[0] != lp[1]).collect()

# See decision tree that we've built
# Semantic model not a black box like neuronal networks, we can understand the tree with its rules
print('Learned Classification tree model:') # We have depth of 5 and 55 nodes
print(model.toDebugString())

"""#Random Forest

- In scikit-learn
"""

# Random forest has a high performance
# Instance and fit separated while in pyspark we do it in the same command line
time0 = time()
nb_estimators = 100 # 100 estimators (number of tress)
RF = RandomForestClassifier(n_estimators=nb_estimators) # Applying Random Forest Classifier
RF.fit(X_train, y_train) # Fit the model
y_pred = RF.predict(X_test) # Predict the model
print(metrics.accuracy_score(y_test, y_pred)) # Accuracy of 0.97
print("time: %s seconds" %(np.round(time()-time0,decimals=2)))

"""- In pySpark + MLlib"""

time0 = time()
# Train a random forest model
from pyspark.mllib.tree import RandomForest
model = RandomForest.trainClassifier(trainingData,numClasses=len(set(y)), categoricalFeaturesInfo={},
                                     impurity='gini', maxDepth=5, numTrees=10)
# Evaluate model on test instances and compute test error
predictions = model.predict(testData.map(lambda x: x.features))
accuracy = metrics.accuracy_score(y_test, predictions.collect())
print('RF Accuracy =' + str(accuracy))

print("time: %s seconds" %(np.round(time()-time0, decimals=2)))

#print('Learned classification tree model:') # If we want to see the decision tree that we've created we can do that
#print(model.toDebugString())

"""#Gradient Boosting

- scikit-learn
"""

# It's not Adaboost, it's a little bit different
# For ensemble methods there are not only Boosting and Bagging, there is also Stack generalization and others
# Stacking or stack generalization we can compile several methods together (decision tree with SVM for ex or something else and it's the way of combination that changes)
time0 = time()
GB = GradientBoostingClassifier(n_estimators=nb_estimators)
GB.fit(X_train, y_train)
y_pred = GB.predict(X_test)
print(metrics.accuracy_score(y_test, y_pred))
print("time: %s seconds" %(np.round(time()-time0,decimals=2)))

"""- In pySpark + MLlib"""

time0 = time()

# Train a GradientBoostedTree model
from pyspark.mllib.tree import GradientBoostedTrees
model = GradientBoostedTrees.trainClassifier(trainingData, categoricalFeaturesInfo={},
                                      maxDepth=5, numIterations=10)
# Evaluate model on test instances and compute test error
predictions = model.predict(testData.map(lambda x: x.features))
accuracy = metrics.accuracy_score(y_test, predictions.collect())
print('GBT Accuracy =' + str(accuracy))

print("time: %s seconds" %(np.round(time()-time0, decimals=2)))

#print('Learned classification tree model:')
#print(model.toDebugString())

# Accuracy very low with pyspark 0.18 while it's 0.94 with scikit learn and we used a high performance algorithm
# This difference comes from the parallelization
# They tried to do an heuristic. Is the algorithm possible to parallelize ? In theory the algorithm can't be parallelized but we try to create a version that is close to the algorithm in order to parallelize that's why the accuracy is low

sc.stop()
